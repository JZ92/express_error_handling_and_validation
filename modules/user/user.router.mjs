/* 
  if there is an error thrown in the DB, asyncMiddleware
  will pass it to next() and express will handle the error */
import raw from "../../middleware/route.async.wrapper.mjs";
import user_model from "./user.model.mjs";
import express from "express";
import log from "@ajar/marker";
import Joi from "joi";

const router = express.Router();

// parse json req.body on post routes
router.use(express.json());

// CREATE NEW USER
router.post(
  "/",
  raw(async (req, res) => {
    log.obj(req.body, "create a user, req.body:");

    const schema = Joi.object({
      first_name: Joi.string().min(3).max(30).required(),
      last_name: Joi.string().min(3).max(30).required(),
      email: Joi.string().email().required(),
    });

    const { error, value } = schema.validate(req.body);
    if (error) {
      throw new Error(
        `Validation error: ${error.details.map((x) => x.message).join(", ")}`
      );
    } else {
      req.body = value;
    }

    const user = await user_model.create(req.body);
    res.status(200).json(user);
  })
);

router.post(
  "/many",
  raw(async (req, res) => {
    log.obj(req.body, "create users, req.body:");

    req.body.forEach((currUser) => {
      const schema = Joi.object({
        first_name: Joi.string().min(3).max(30).required(),
        last_name: Joi.string().min(3).max(30).required(),
        email: Joi.string().email().required(),
      });

      const { error, value } = schema.validate(currUser);
      if (error) {
        throw new Error(
          `Validation error: ${error.details.map((x) => x.message).join(", ")}`
        );
      } else {
        req.body = value;
      }
    });

    const user = await user_model.insertMany(req.body);
    res.status(200).json(user);
  })
);

// GET ALL USERS
router.get(
  "/",
  raw(async (req, res) => {
    const users = await user_model.find();
    res.status(200).json(users);
  })
);

router.get(
  "/paginate/:page?/:items?",
  raw(async (req, res) => {
    log.obj(req.params, "get all users, req.params:");
    let { page = 0, items = 10 } = req.params;
    const users = await user_model
      .find()
      .select(`first_name last_name email phone`)
      .limit(parseInt(items))
      .skip(parseInt(page * items));

    res.status(200).json(users);
  })
);

// GETS A SINGLE USER
router.get(
  "/:id",
  raw(async (req, res) => {
    const user = await user_model.findById(req.params.id);
    res.status(200).json(user);
  })
);
// UPDATES A WHOLE USER
router.put(
  "/:id",
  raw(async (req, res) => {
    const schema = Joi.object({
      first_name: Joi.string().min(3).max(30),
      last_name: Joi.string().min(3).max(30),
      email: Joi.string().email(),
    });

    const { error, value } = schema.validate(req.body);
    if (error) {
      throw new Error(
        `Validation error: ${error.details.map((x) => x.message).join(", ")}`
      );
    } else {
      req.body = value;
    }

    const user = await user_model.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
      upsert: false,
    });
    res.status(200).json(user);
  })
);

// DELETES A USER
router.delete(
  "/:id",
  raw(async (req, res) => {
    const user = await user_model.findByIdAndRemove(req.params.id);
    res.status(200).json(user);
  })
);

export default router;
